(function(){

  function getLineHeight(el) {
    var lh = window.getComputedStyle(el).lineHeight;
    if (lh === 'normal') {
      lh = parseInt(window.getComputedStyle(el).fontSize.replace('px', '') * 1.2);
    } else {
      lh = parseInt(lh.replace('px', ''));
    }
    return lh;
  }

  function toggelContent(container, content, lines, lineHeight, event) {
    if (!content.hasAttribute('data-showless')) {
      content.style.maxHeight = 3 * lineHeight + 'px';
      content.setAttribute('data-showless', '');
      container.querySelector('#btn1').textContent = 'read more';
    } else {
      content.style.maxHeight = lines * lineHeight + 'px';
      content.removeAttribute('data-showless');
      container.querySelector('#btn1').textContent = 'show less';
    }
  }

  document.addEventListener("DOMContentLoaded", function (event) {
    var elements = document.querySelectorAll('[data-expandable]');
    elements.forEach(function (element) {
      var height = element.clientHeight;
      var lineHeight = getLineHeight(element);
      var lines = Math.max(Math.floor(height/lineHeight), 0);

      if (lines > 3) {
        element.innerHTML = '<div class="text-animation" id="overflow">' + element.innerHTML +
        '</div><div><a href="#" id="btn1">read more</button></a>';

        var content = element.querySelector('#overflow');

        content.style.maxHeight = 3 * lineHeight + 'px';
        content.setAttribute('data-showless', '');

        clickHandler = toggelContent.bind(null, element, content, lines, lineHeight);

        element.querySelector('#btn1').addEventListener('click', clickHandler);
      }

    });
  });
})();
