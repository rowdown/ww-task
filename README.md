# Westwing Task

## Task 1
Refactor Code jQuery code.
Assumed `/get-users-endpoint` can return multple user info with json containing user `id` .

## Task 2
Refactored jQuery ES5 code to ES6

## Task 3
Implement small expandable block using HTML + CSS + JAVASCRIPT
### To Run
```shell
cd task3/
index.html   # Open in Browser
```

## Task 3
Implement small expandable block in React.
### Install
```shell
cd task4/
npm install
```
### To Run
```shell
npm start    # Compiles the app and opens it in a browser
```