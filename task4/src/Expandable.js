import React, { Component } from "react";
import PropTypes from 'prop-types';

function renderButton(expanded, moreTextLabel, lessTextLabel, toggleDisplay) {
  return (
    <div>
      {!expanded ? (
        <button onClick={toggleDisplay}>{moreTextLabel}</button>
      ) : (
        <button onClick={toggleDisplay}>{lessTextLabel}</button>
      )}
    </div>
  );
}

class Expandable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      showButton: false,
      lines: 0,
      lineHeight: 0,
      moreTextLabel: props.moreTextLabel,
      lessTextLabel: props.lessTextLabel
    };
    this.toggleDisplay = this.toggleDisplay.bind(this);
  }

  componentDidMount() {
    let lineHeight = this.getLineHeight();
    let height = this.refs.textContainer.clientHeight;
    let lines = height / lineHeight;
    if (lines > 3) {
      this.setState({
        showButton: true,
        lines: lines,
        lineHeight: lineHeight
      });
    }
    this.refs.textContainer.style.maxHeight = 3 * lineHeight + "px";
  }

  toggleDisplay() {
    if (this.state.expanded) {
      this.refs.textContainer.style.maxHeight =
        3 * this.state.lineHeight + "px";
    } else {
      this.refs.textContainer.style.maxHeight =
        this.state.lines * this.state.lineHeight + "px";
    }
    this.setState({
      expanded: !this.state.expanded
    });
  }

  getLineHeight() {
    let lh = window.getComputedStyle(this.refs.textContainer).lineHeight;
    if (lh === "normal") {
      lh = parseInt(
        window
          .getComputedStyle(this.refs.textContainer)
          .fontSize.replace("px", "") * 1.2
      );
    } else {
      lh = parseInt(lh.replace("px", ""));
    }
    return lh;
  }

  render() {
    const { expanded, moreTextLabel, lessTextLabel, showButton } = this.state;
    const { content } = this.props;
    const button = renderButton(
      expanded,
      moreTextLabel,
      lessTextLabel,
      this.toggleDisplay
    );
    return (
      <div>
        <div className="contentDiv" ref="textContainer">
          {content}
        </div>
        {showButton && button}
      </div>
    );
  }
}

Expandable.defaultProps = {
  moreTextLabel: 'read more',
  lessTextLabel: 'show less'
};

Expandable.propTypes = {
  moreTextLabel: PropTypes.string,
  lessTextLabel: PropTypes.string,
  content: PropTypes.string
};

export default Expandable;
