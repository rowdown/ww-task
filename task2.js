import _ from 'lodash';
import jquery from 'jquery';

class MODULE {
  constructor() {
    this.init();
  }

  init() {
    jquery('.elements').each(function () {
      let ele = jquery(this);
      let id = ele.attr('data-id');
      ele.on('click', () => {
        jquery.ajax({
          url: '/change-status',
          method: 'GET',
          data: {
            id: id
          },
          dataType: 'json',
          success: (data) => {
            if (data.success) this.setElementStatus(id, data.element.statusText);
          }
        });
      });
    });
  }

  setElementStatus(id, statusText) {
    jquery('.elements').each( () => {
      let ele = jquery(this);
      if (ele.attr('data-id') === id) {
        ele.find('.status').text(statusText);
      });
  });
}
}

export default MODULE;
