var userIds = new Array();
var $users = $('.userNode');

function getUserInfo(userIds, $userSuccess, userErorr) {
  $.ajax({
    url: '/get-users-endpoint',
    method: 'POST',
    data: {
      userIds: userIds
    },
    dataType: 'json',
    success: userSuccess,
    error: userErorr
  });
}

function setUserLoading($user) {
  var userName = $user.find( '.name' );
  userName.css({ 'font-weight': 600, 'padding-left': '20px' });
  userName.text(userName.text() + ' (loading)');
}

function setUserData($user, data) {
  data.users.forEach(function (user) {
    $user = $users.find('[data-slide="' + user.id + '"]');
    $user.text(user.userName);
    $user.append('<div class=userInfo></div>');
    $user.find('.userInfo').text(user.userInfo);
    $user.css({ 'font-weight': 300, 'padding-left': '0px' });
  });
}

function removeUserLoading($user) {
  $user.each(function (index) {
    var $this = $( this );
    var userName = user.find( '.name' );
    userName.text(userName.text().replace(' (loading)', ''));
  });
}

var userErorr = removeUserLoading.bind(null, $user);
var userSuccess = setUserData.bind(null, $user);

$users.each(function (index) {
  var $this = $( this );
  if ($this.is(':visible')) {
    setUserLoading($this);
  }
  userIds.push($this.attr('data-id'));
});

getUserInfo(userIds, userSuccess, userErorr);
